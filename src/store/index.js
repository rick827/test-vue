import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export const store =  new Vuex.Store({
  state: {
    valueName: '',
    valueSecondNames: ''
  },
  mutations: {
    change(state, valueName, valueSecondNames){
      state.valueName = valueName,
      state.valueSecondNames = valueSecondNames
    }
  },
  getters:{
    valueName: state => state.valueName,
    valueSecondNames: state => state.valueSecondNames
  },
  actions: {
  },
  modules: {
  }
})
